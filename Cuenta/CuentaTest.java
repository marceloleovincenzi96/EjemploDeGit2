import org.junit.Assert;
import org.junit.Test;

public class CuentaTest {

	@Test
	public void test1() {
		Cuenta caja = new CajaDeAhorro("32", "pepe", 20);

		Assert.assertEquals("pepe", caja.getNombreTitular());

	}

	@Test
	public void comprobarSaldo() {
		Cuenta caja = new CajaDeAhorro("32", "pepe", 20);

		Assert.assertEquals(20, caja.getSaldo(), 0.01);

	}

	@Test
	public void probarSaldo() {

		CajaDeAhorro caja = new CajaDeAhorro("11111111", "Juan", 100);
		caja.aplicarInteres();
		Assert.assertEquals(105, caja.getSaldo(), 0.01);
	}

	@Test
	public void Transferir() {

		CuentaCorriente caja = new CuentaCorriente("11111111", "Juan", 200);
		CuentaCorriente caja2 = new CuentaCorriente("11111121", "Juan", 200);
		caja.transferencia(100, caja2);
		Assert.assertEquals(97, caja.getSaldo(), 0.01);

		Assert.assertEquals(303, caja2.getSaldo(), 0.01);
	}

}
