class Cuenta {

	private String dniTitular;
	private String nombreTitular;
	private double saldo;

	public Cuenta(String dni, String nombre, double saldo) {
		dniTitular = dni;
		nombreTitular = nombre;
		this.saldo = saldo;

	}

	public Cuenta(String dni, String nombre) {

		dniTitular = dni;
		nombreTitular = nombre;

	}

	public String getDniTitular() {
		return dniTitular;
	}

	public void setDniTitular(String dniTitular) {
		this.dniTitular = dniTitular;
	}

	public String getNombreTitular() {
		return nombreTitular;
	}

	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

}
