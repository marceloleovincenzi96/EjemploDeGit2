
public class CajaDeAhorro extends Cuenta {
	
	final double tasaInteres = 0.05;
	
	
	public CajaDeAhorro (String dni, String nombre, double saldo){
		
		super (dni, nombre, saldo);
	}
	
	public void aplicarInteres(){
		
		super.setSaldo(super.getSaldo() + (super.getSaldo() * this.tasaInteres));
		
		
	}

}
 