
class CuentaCorriente extends Cuenta{
	
	
	
	double comision = 0.03;
	
	public CuentaCorriente (String dni, String nombre, double saldo){
		super (dni, nombre, saldo);
	}
	
	public void transferencia(double montoTransferir, Cuenta cuenta){
		
		 double monto = montoTransferir + (montoTransferir * this.comision);
		 
		 super.setSaldo(super.getSaldo() - monto);
		 
		 cuenta.setSaldo (cuenta.getSaldo() + monto);
		
	}

	
	
}
